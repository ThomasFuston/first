(ns app.core
  (:require
   [uix.core :as uix :refer [defui]]
   [uix.dom]))

(defui button [{:keys [on-click children]}]
  ($ :button.btn {:on-click on-click}
     children))

(defui header []
  ($ :header
     ($ :h1 "Logo")))

(defui app []
  (let [[state set-state!] (uix.core/use-state 0)]
    ($ :<>
       ($ header)
       ($ button {:on-click #(set-state! dec)} "-")
       ($ :span state)
       ($ button {:on-click #(set-state! inc)} "+"))))


(defonce root
  (uix.dom/create-root (js/document.getElementById "app")))

(defn render []
  (uix.dom/render-root ($ app) root))

(defn ^:export init []
  (render))
